<?php

namespace WarehouseX\User\Model\UserWarehouse;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * UserWarehouse.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $warehouseId = null;

    public $user = null;

    public $department = null;
}
