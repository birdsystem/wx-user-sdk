<?php

namespace WarehouseX\User\Model\UserWarehouse;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * UserWarehouse.
 */
class Write extends AbstractModel
{
    /**
     * @var int
     */
    public $warehouseId = null;

    public $user = null;

    public $department = null;
}
