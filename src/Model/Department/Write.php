<?php

namespace WarehouseX\User\Model\Department;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Department.
 */
class Write extends AbstractModel
{
    /**
     * @var int|null
     */
    public $parentId = null;

    /**
     * @var int
     */
    public $userIdManager = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $telephone = null;
}
