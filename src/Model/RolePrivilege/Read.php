<?php

namespace WarehouseX\User\Model\RolePrivilege;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * RolePrivilege.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    public $role = null;

    public $privilege = null;
}
