<?php

namespace WarehouseX\User\Model\RolePrivilege;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * RolePrivilege.
 */
class Write extends AbstractModel
{
    public $role = null;

    /**
     * @var string|null
     */
    public $privilege = null;
}
