<?php

namespace WarehouseX\User\Model\UserRole;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * UserRole.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    public $user = null;

    public $role = null;
}
