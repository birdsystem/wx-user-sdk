<?php

namespace WarehouseX\User\Model\UserRole;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * UserRole.
 */
class Write extends AbstractModel
{
    public $user = null;

    public $role = null;
}
