<?php

namespace WarehouseX\User\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class Token extends AbstractModel
{
    /**
     * @var string
     */
    public $token = null;
}
