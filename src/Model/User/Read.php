<?php

namespace WarehouseX\User\Model\User;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * User.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    public $client = null;

    public $department = null;

    /**
     * @var \WarehouseX\User\Model\Role\Read[]
     */
    public $userRoles = null;

    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string|null
     */
    public $firstName = null;

    /**
     * @var string|null
     */
    public $lastName = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string[]
     */
    public $roles = null;

    /**
     * @var string
     */
    public $companyName = null;
}
