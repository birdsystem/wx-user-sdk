<?php

namespace WarehouseX\User\Model\User;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * User.
 */
class Write extends AbstractModel
{
    /**
     * @var int|null
     */
    public $clientId = null;

    public $client = null;

    public $department = null;

    /**
     * @var \WarehouseX\User\Model\Role\Write[]
     */
    public $userRoles = null;

    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string
     */
    public $newPassword = null;

    /**
     * @var string
     */
    public $oldPassword = null;

    /**
     * @var string|null
     */
    public $firstName = null;

    /**
     * @var string|null
     */
    public $lastName = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $status = null;
}
