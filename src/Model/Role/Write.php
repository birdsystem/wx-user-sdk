<?php

namespace WarehouseX\User\Model\Role;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Role.
 */
class Write extends AbstractModel
{
    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $status = null;
}
