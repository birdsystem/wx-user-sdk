<?php

namespace WarehouseX\User\Model\Role;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Role.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $status = null;

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';
}
