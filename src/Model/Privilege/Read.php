<?php

namespace WarehouseX\User\Model\Privilege;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Privilege.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $route = null;

    /**
     * @var string
     */
    public $method = 'GET';

    /**
     * @var string
     */
    public $module = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $operation = null;
}
