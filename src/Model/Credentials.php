<?php

namespace WarehouseX\User\Model;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class Credentials extends AbstractModel
{
    /**
     * @var string
     */
    public $username = null;

    /**
     * @var string
     */
    public $password = null;
}
