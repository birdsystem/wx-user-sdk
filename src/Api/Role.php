<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\Role\Read as Read;
use WarehouseX\User\Model\Role\Write as Write;

class Role extends AbstractAPI
{
    /**
     * Retrieves the collection of Role resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'name'	string
     *                       'note'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[name]'	string
     *                       'order[note]'	string
     *                       'order[type]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getRoleCollection',
        'GET',
        'api/user/roles',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Role resource.
     *
     * @param Write $Model The new Role resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postRoleCollection',
        'POST',
        'api/user/roles',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Role resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getRoleItem',
        'GET',
        "api/user/roles/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Role resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated Role resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putRoleItem',
        'PUT',
        "api/user/roles/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Role resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteRoleItem',
        'DELETE',
        "api/user/roles/$id",
        null,
        [],
        []
        );
    }
}
