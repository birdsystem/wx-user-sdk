<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\Privilege\Read as Read;

class Privilege extends AbstractAPI
{
    /**
     * Retrieves the collection of Privilege resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getPrivilegeCollection',
        'GET',
        'api/user/privileges',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Privilege resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getPrivilegeItem',
        'GET',
        "api/user/privileges/$id",
        null,
        [],
        []
        );
    }
}
