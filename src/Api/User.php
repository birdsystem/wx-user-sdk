<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\User\Read as Read;
use WarehouseX\User\Model\User\Write as Write;

class User extends AbstractAPI
{
    /**
     * Get User Privileges.
     */
    public function getPrivileges()
    {
        return $this->request(
        'getPrivileges',
        'GET',
        'api/user/get-privileges',
        null,
        [],
        []
        );
    }

    /**
     * Get User Profile.
     */
    public function getProfile()
    {
        return $this->request(
        'getUserProfile',
        'GET',
        'api/user/profile',
        null,
        [],
        []
        );
    }

    /**
     * Create new JWT Token.
     *
     * @return mixed
     */
    public function getJwtToken(): mixed
    {
        return $this->request(
        'getJwtToken',
        'POST',
        'api/user/token',
        null,
        [],
        []
        );
    }

    /**
     * Retrieves the collection of User resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'username'	string
     *                       'title'	string
     *                       'firstName'	string
     *                       'lastName'	string
     *                       'telephone'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'department.name'	string
     *                       'userRoles.name'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[username]'	string
     *                       'order[title]'	string
     *                       'order[firstName]'	string
     *                       'order[lastName]'	string
     *                       'order[telephone]'	string
     *                       'order[type]'	string
     *                       'order[status]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *                       'order[department.name]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getUserCollection',
        'GET',
        'api/user/users',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a User resource.
     *
     * @param Write $Model The new User resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postUserCollection',
        'POST',
        'api/user/users',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a User resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getUserItem',
        'GET',
        "api/user/users/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the User resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated User resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putUserItem',
        'PUT',
        "api/user/users/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the User resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteUserItem',
        'DELETE',
        "api/user/users/$id",
        null,
        [],
        []
        );
    }
}
