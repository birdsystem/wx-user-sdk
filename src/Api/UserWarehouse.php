<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\UserWarehouse\Read as Read;
use WarehouseX\User\Model\UserWarehouse\Write as Write;

class UserWarehouse extends AbstractAPI
{
    /**
     * Retrieves the collection of UserWarehouse resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getUserWarehouseCollection',
        'GET',
        'api/user/user_warehouses',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a UserWarehouse resource.
     *
     * @param Write $Model The new UserWarehouse resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postUserWarehouseCollection',
        'POST',
        'api/user/user_warehouses',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a UserWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getUserWarehouseItem',
        'GET',
        "api/user/user_warehouses/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the UserWarehouse resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated UserWarehouse resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putUserWarehouseItem',
        'PUT',
        "api/user/user_warehouses/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the UserWarehouse resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteUserWarehouseItem',
        'DELETE',
        "api/user/user_warehouses/$id",
        null,
        [],
        []
        );
    }
}
