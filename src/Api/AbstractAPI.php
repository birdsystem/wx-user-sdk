<?php

namespace WarehouseX\User\Api;

use OpenAPI\Runtime\AbstractAPI as BaseClass;
use WarehouseX\User\ResponseHandlerStack;

class AbstractAPI extends BaseClass implements APIInterface
{
    protected string $responseHandlerStackClass = ResponseHandlerStack::class;

    public function __construct(?HttpClientInterface $client = null)
    {
        parent::__construct($client);
    }
}
