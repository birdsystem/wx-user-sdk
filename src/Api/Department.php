<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\Department\Read as Read;
use WarehouseX\User\Model\Department\Write as Write;

class Department extends AbstractAPI
{
    /**
     * Retrieves the collection of Department resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'name'	string
     *                       'type'	string
     *                       'type[]'	array
     *                       'telephone'	string
     *                       'id'	integer
     *                       'id[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'order[id]'	string
     *                       'order[clientId]'	string
     *                       'order[name]'	string
     *                       'order[type]'	string
     *                       'order[telephone]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getDepartmentCollection',
        'GET',
        'api/user/departments',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Department resource.
     *
     * @param Write $Model The new Department resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postDepartmentCollection',
        'POST',
        'api/user/departments',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Department resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getDepartmentItem',
        'GET',
        "api/user/departments/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Department resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated Department resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putDepartmentItem',
        'PUT',
        "api/user/departments/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Department resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteDepartmentItem',
        'DELETE',
        "api/user/departments/$id",
        null,
        [],
        []
        );
    }
}
