<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\RolePrivilege\Read as Read;
use WarehouseX\User\Model\RolePrivilege\Write as Write;

class RolePrivilege extends AbstractAPI
{
    /**
     * Retrieves the collection of RolePrivilege resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getRolePrivilegeCollection',
        'GET',
        'api/user/role_privileges',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a RolePrivilege resource.
     *
     * @param Write $Model The new RolePrivilege resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postRolePrivilegeCollection',
        'POST',
        'api/user/role_privileges',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a RolePrivilege resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getRolePrivilegeItem',
        'GET',
        "api/user/role_privileges/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the RolePrivilege resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated RolePrivilege resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putRolePrivilegeItem',
        'PUT',
        "api/user/role_privileges/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the RolePrivilege resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteRolePrivilegeItem',
        'DELETE',
        "api/user/role_privileges/$id",
        null,
        [],
        []
        );
    }
}
