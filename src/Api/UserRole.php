<?php

namespace WarehouseX\User\Api;

use WarehouseX\User\Model\UserRole\Read as Read;
use WarehouseX\User\Model\UserRole\Write as Write;

class UserRole extends AbstractAPI
{
    /**
     * Retrieves the collection of UserRole resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getUserRoleCollection',
        'GET',
        'api/user/user_roles',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a UserRole resource.
     *
     * @param Write $Model The new UserRole resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postUserRoleCollection',
        'POST',
        'api/user/user_roles',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a UserRole resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getUserRoleItem',
        'GET',
        "api/user/user_roles/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the UserRole resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated UserRole resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putUserRoleItem',
        'PUT',
        "api/user/user_roles/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the UserRole resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteUserRoleItem',
        'DELETE',
        "api/user/user_roles/$id",
        null,
        [],
        []
        );
    }
}
