<?php

namespace WarehouseX\User;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getDepartmentCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\Department\\Read[]',
        ],
        'postDepartmentCollection' => [
            '201.' => 'WarehouseX\\User\\Model\\Department\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getDepartmentItem' => [
            '200.' => 'WarehouseX\\User\\Model\\Department\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putDepartmentItem' => [
            '200.' => 'WarehouseX\\User\\Model\\Department\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteDepartmentItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getPrivilegeCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\Privilege\\Read[]',
        ],
        'getPrivilegeItem' => [
            '200.' => 'WarehouseX\\User\\Model\\Privilege\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getRolePrivilegeCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\RolePrivilege\\Read[]',
        ],
        'postRolePrivilegeCollection' => [
            '201.' => 'WarehouseX\\User\\Model\\RolePrivilege\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getRolePrivilegeItem' => [
            '200.' => 'WarehouseX\\User\\Model\\RolePrivilege\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putRolePrivilegeItem' => [
            '200.' => 'WarehouseX\\User\\Model\\RolePrivilege\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteRolePrivilegeItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getRoleCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\Role\\Read[]',
        ],
        'postRoleCollection' => [
            '201.' => 'WarehouseX\\User\\Model\\Role\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getRoleItem' => [
            '200.' => 'WarehouseX\\User\\Model\\Role\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putRoleItem' => [
            '200.' => 'WarehouseX\\User\\Model\\Role\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteRoleItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getJwtToken' => [
            '200.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserRoleCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\UserRole\\Read[]',
        ],
        'postUserRoleCollection' => [
            '201.' => 'WarehouseX\\User\\Model\\UserRole\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserRoleItem' => [
            '200.' => 'WarehouseX\\User\\Model\\UserRole\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putUserRoleItem' => [
            '200.' => 'WarehouseX\\User\\Model\\UserRole\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteUserRoleItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserWarehouseCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\UserWarehouse\\Read[]',
        ],
        'postUserWarehouseCollection' => [
            '201.' => 'WarehouseX\\User\\Model\\UserWarehouse\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserWarehouseItem' => [
            '200.' => 'WarehouseX\\User\\Model\\UserWarehouse\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putUserWarehouseItem' => [
            '200.' => 'WarehouseX\\User\\Model\\UserWarehouse\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteUserWarehouseItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserCollection' => [
            '200.' => 'WarehouseX\\User\\Model\\User\\Read[]',
        ],
        'postUserCollection' => [
            '201.' => 'WarehouseX\\User\\Model\\User\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserItem' => [
            '200.' => 'WarehouseX\\User\\Model\\User\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putUserItem' => [
            '200.' => 'WarehouseX\\User\\Model\\User\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteUserItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
